package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
)

func main(){
	r := gin.Default()

	//handle encrypt text
	r.POST("/encrypt", func(c *gin.Context) {
		var request struct {
			PlainText string `json:"plainText"`
			Key string `json:"key"`
		}
		c.BindJSON(&request)
		cipherText,err := encrypt(request.PlainText,request.Key)
		if err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}
		c.JSON(200, gin.H{
			"plainText": request.PlainText,
			"key": request.Key,
			"cipherText": cipherText,
		})
	})

	//handle decrypt text
	r.POST("/decrypt", func(c *gin.Context) {
		var request struct {
			CipherText string `json:"cipherText"`
			Key string `json:"key"`
		}
		c.BindJSON(&request)
		plainText,err := decrypt(request.CipherText,request.Key)
		if err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}
		c.JSON(200, gin.H{
			"plainText": plainText,
			"key": request.Key,
			"cipherText": request.CipherText,
		})
	})
	r.Run("localhost:9000")


}



func decrypt(cipherText string, key string) (string, error) {
	keyByteArr :=  []byte(key)
	cipherTextByteArr,err := hex.DecodeString(cipherText)
	block, err := aes.NewCipher(keyByteArr)
	if err != nil {
		return "",err
	}
	if len(cipherTextByteArr) < aes.BlockSize {
		return "", fmt.Errorf("ciphertext too short")
	}
	iv := cipherTextByteArr[:aes.BlockSize]
	if len(cipherTextByteArr)%aes.BlockSize != 0 {
		return "", fmt.Errorf("ciphertext is not a multiple of the block size")
	}
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherTextByteArr[aes.BlockSize:], cipherTextByteArr[aes.BlockSize:])
	return string(cipherTextByteArr[aes.BlockSize:]),err
}



func encrypt(plainText string, key string)(string,error){
	keyByteArr :=  []byte(key)
	plainTextByteArr := []byte(plainText)
	if len(plainTextByteArr)%aes.BlockSize != 0 {
		return "", fmt.Errorf("ciphertext is not a multiple of the block size")
	}
	block, err := aes.NewCipher(keyByteArr)
	if err != nil {
		return "",err
	}
	cipherText := make([]byte, aes.BlockSize+len(plainTextByteArr))
	ivByteArr := cipherText[:aes.BlockSize]

	if _, err := io.ReadFull(rand.Reader, ivByteArr); err != nil {
		return "",err
	}
	mode := cipher.NewCBCEncrypter(block, ivByteArr)
	mode.CryptBlocks( cipherText[aes.BlockSize:],plainTextByteArr)
	return  hex.EncodeToString(cipherText),nil
}
